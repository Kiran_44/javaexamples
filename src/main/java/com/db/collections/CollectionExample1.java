package com.db.collections;

import java.util.*;

/***
 * ADS(Abstract DataStructures)
 */
public class CollectionExample1 {

    public static void main(String[] args){

        //testSet();
        testMap();
    }
    public static void testLists(){
        List list=new ArrayList();
        list.add("QQQ");
        System.out.println(list);
    }

    public  static void testSet(){
        Set set=new HashSet();
        set.add("Kiran");
        set.add("Lion");
        set.add("Kiran");

        System.out.println(set);

    }

    public static void testMap(){
        Map<String,String> users=new HashMap<String,String>();
        users.put("K","kkkk");
        users.put("L","lll");

        System.out.println(users.get("K"));
    }
}
