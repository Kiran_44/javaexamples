package com.db.lambdafunctions;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.stream.Collectors;

public class AirportsFunctionDemo {
    public static void main(String[] args) throws Exception {
        List<String[]> helipads = Files.readAllLines(Paths.get("G:\\Downloads_2\\airports.csv")).
                stream().
                skip(1).
                map(AirportsFunctionDemo::transformByComma).
                filter(AirportsFunctionDemo::findHeliPorts).
                collect(Collectors.toList());

        System.out.println(helipads.size());
    }
    public static  String[] transformByComma(String data){
        return data.split(",");

    }

    public static boolean findHeliPorts(String[] data) {
        return data[2].equals("\"heliport\"");
    }



}
