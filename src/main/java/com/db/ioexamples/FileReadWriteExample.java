package com.db.ioexamples;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileReadWriteExample {

    public  static void main(String[] args){
        long startTime = System.currentTimeMillis();
        //readFile();
        //readFileUsingReader();
        readFilesNativeWithStreams();
        long endTime = System.currentTimeMillis();
        System.out.println(endTime-startTime);



    }

    public static void readFile() {
        InputStream is=null;
        try {
            is = new FileInputStream("C:/Users/Kiran/Desktop/example.txt");
            byte[] buffer = new byte[is.available()];
            is.read(buffer);
            System.out.println(new String(buffer));
        } catch (Exception e) {
            System.out.println(e);
        }
        finally {
            if(is!=null)
                try {
                    is.close();
                }
                catch(IOException e){
                        //TODO
            }
        }
    }


//    public static void readFileUsingReader(){
//        String data="";
//        try{
//            BufferedReader reader=new BufferedReader(new FileReader());
//            while ((data=reader.readLine())!=null)
//                System.out.println(data);
//
//        }catch (Exception e){
//            System.out.println(e);
//        }
//
//    }

    public static void readFileUsingReader() {
        // a varaible to store each line
        String data = "";
        // the buffered reader
        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader("G:/Downloads_2/airports.csv"));
            while ((data = reader.readLine()) != null) {
                System.out.println(data);
            }
        } catch (Exception fe) {
            System.out.println(fe);
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public static void readFilesNativeWithStreams(){
        try {
            System.out.println(Files.readAllLines(Paths.get("G:/Downloads_2/airports.csv")));

        }catch (Exception e)
        {}
        finally {

        }
    }

}
