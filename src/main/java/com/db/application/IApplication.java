package com.db.application;

public interface IApplication {

    /**
     *  Returns the version of application
     *
     * @return String
     */
    String getVersion();

    /**
     * @param accountid
     * @param amount
     */
    double deposit(String accountid,double amount);


    /**
     * @param accountid
     * @param amount
     */

    double withdraw(String accountid,double amount);
}
