package com.db.application;

public class ApplicationLauncher {

    public static void main(String[] args){

//        System.out.println(new Applicationimpl() instanceof IApplication);
//        IApplication application=new Applicationimpl();
//
//        System.out.println(application.deposite("0001",1000));
//        System.out.println(application.withdraw("0001",900));

        /**
         * Disengage with the implementation .....
         * Disengage with the Classes for business implementation only work with interfaces
         * Decoupling will help in enchancement of the future proof coding
         */
        IApplication application1 = ApplicationFactory.getApplication();
        application1.deposit("xx", 1);

    }
}
