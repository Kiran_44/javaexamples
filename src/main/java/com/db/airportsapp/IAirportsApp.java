package com.db.airportsapp;

import java.util.List;

public interface IAirportsApp {
    List<Airport> findAirportByCode(String code);

    List<Airport> findAirportByName(String name);

    List<Airport> findAirportByLatitude(String latitude);

    List<Airport>findAirportByLongitude(String longitude);

    List<Airport>findAirportRandomly();

    //TODO
    //List<Airport> findByAddress();
    List<Airport> findAirportByAddress(String address);

    List<String>  findAirportNearMe(String location);

    long countTotalAirports();

    void login();
    void signup();
    void  help();
    void siteInfo();





}
