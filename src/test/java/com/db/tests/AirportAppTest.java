package com.db.tests;

import com.db.airportsapp.Airport;
import com.db.airportsapp.AirportAppFactory;
import com.db.airportsapp.IAirportsApp;
import jdk.jshell.spi.ExecutionControl;
import org.junit.Test;

import java.net.NetPermission;
import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

public class AirportAppTest {
    @Test
    public void testFindAirportByCode() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByCode("6523");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByCodeBadArgument() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByCode(null);
    }

    @Test
    public void testFindAirportByName() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByName("Lowell Field");
        assertFalse(actualAirports.isEmpty());

    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByNameBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByName(null);
    }

    @Test
    public void testFindAirportByLatitude() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLatitude("35.408087");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByLatitudeBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLatitude(null);
    }

    @Test
    public void testFindAirportByLongitude() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLongitude("-97.8180194");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByLongitudeBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByLongitude(null);
    }

    @Test
    public void testFindAirportRandomly() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportRandomly();
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = NullPointerException.class)
    public void testFindAirportNearMe() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<String> actualAirports = app.findAirportNearMe(null);

    }

    @Test
    public void testFindAirportByAddress()
    {

        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByAddress("Bensalem");
        assertFalse(actualAirports.isEmpty());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testFindAirportByAddressBadArguments() {
        IAirportsApp app = AirportAppFactory.getInstance();
        List<Airport> actualAirports = app.findAirportByName(null);
    }

    @Test(expected = NullPointerException.class)
    public void testCountTotalAirports() {

        IAirportsApp app = AirportAppFactory.getInstance();
        Long actualAirports = app.countTotalAirports();
    }

    @Test(expected = NullPointerException.class)
    public void testLogin() {

        IAirportsApp app = AirportAppFactory.getInstance();
        app.login();
    }

    @Test(expected = NullPointerException.class)
    public void testSignUp() {
        IAirportsApp app = AirportAppFactory.getInstance();
        app.signup();
    }

    @Test(expected = NullPointerException.class)
    public void testHelp() {
        IAirportsApp app = AirportAppFactory.getInstance();
        app.help();
    }

    @Test(expected = NullPointerException.class)
    public void testSiteInfo() {

        IAirportsApp app = AirportAppFactory.getInstance();
        app.siteInfo();
    }
}
